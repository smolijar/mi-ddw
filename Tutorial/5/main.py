import pandas as pd
from collections import Counter
import math

df = pd.read_csv("./zoo.csv")


# df = pd.read_csv("./bank-data.csv")



def frequentItems(transactions, support):
    counter = Counter()
    for trans in transactions:
        counter.update(frozenset([t]) for t in trans)
    return set(item for item in counter if counter[item] / len(transactions) >= support), counter


def generateCandidates(L, k):
    candidates = set()
    for a in L:
        for b in L:
            union = a | b
            if len(union) == k and a != b:
                candidates.add(union)
    return candidates


def filterCandidates(transactions, itemsets, support):
    counter = Counter()
    for trans in transactions:
        subsets = [itemset for itemset in itemsets if itemset.issubset(trans)]
        counter.update(subsets)
    return set(item for item in counter if counter[item] / len(transactions) >= support), counter


def apriori(transactions, support):
    result = list()
    resultc = Counter()
    candidates, counter = frequentItems(transactions, support)
    result += candidates
    resultc += counter
    k = 2
    while candidates:
        candidates = generateCandidates(candidates, k)
        candidates, counter = filterCandidates(transactions, candidates, support)
        result += candidates
        resultc += counter
        k += 1
    resultc = {item: (resultc[item] / len(transactions)) for item in resultc}
    return result, resultc


def genereateRules(frequentItemsets, supports, minConfidence, maxLen):
    rules = []
    for itemSet in frequentItemsets:
        if (len(itemSet) > 1 and len(itemSet) < maxLen):
            for item in itemSet:
                lhs = set(itemSet)
                lhs.remove(item)
                lhssup, unionsup, rhssup = supports[frozenset(lhs)], supports[itemSet], supports[frozenset([item])]
                conf = unionsup / lhssup
                rule = str(lhs) + " => " + str(item)
                if (conf >= minConfidence):
                    rules.append([rule, {
                        'confidence': conf,
                        'lift': unionsup / (lhssup * rhssup),
                        'conviction': math.inf if conf == 1 else (1 - rhssup) / (1 - conf),
                    }])
    for r in sorted(rules, key=lambda x: x[1]['confidence'], reverse=True):
        print(round(r[1]['confidence'], 2), r[0], '\t\t\t', r[1])


# del df["id"]
# df["income"] = pd.cut(df["income"], 10)
dataset = []
for index, row in df.iterrows():
    row = [col + "=" + str(row[col]) for col in list(df)]
    dataset.append(row)
frequentItemsets, supports = apriori(dataset, 0.3)
print(frequentItemsets)
genereateRules(frequentItemsets, supports, 0.5, 3)
