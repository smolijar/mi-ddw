import nltk
# nltk.download()

from nltk.corpus import brown

# print(brown.words()[0:10])
# brown.tagged_words()[0:10]
# print(len(brown.words()))
# print(dir(brown))


def tokenCounts(tokens):
    from collections import Counter
    counts = Counter(tokens)
    sortedCounts = sorted(counts.items(), key=lambda count:count[1], reverse=True)
    return sortedCounts

def stripPunctuation(tokens):
    from string import punctuation
    return [token for token in tokens if token not in punctuation]

def stripStopwords(tokens):
    from nltk.corpus import stopwords
    return [token for token in tokens if token not in stopwords.words('english')]

def strip(tokens):
    return stripStopwords(stripPunctuation(tokens))

def tag(tokens):
    return nltk.pos_tag(tokens)

def stemAndLem(tokens):
    from nltk.stem.porter import PorterStemmer
    from nltk.stem import WordNetLemmatizer
    stemmer = PorterStemmer()
    stemmed = [stemmer.stem(token) for token in tokens]
    lemmatizer = WordNetLemmatizer()
    return [lemmatizer.lemmatize(token) for token in tokens]

def extractEntities(tagged):
    ne_chunked = nltk.ne_chunk(tagged, binary=False)
    data = {}
    for entity in ne_chunked:
        if isinstance(entity, nltk.tree.Tree):
            text = " ".join([word for word, tag in entity.leaves()])
            ent = entity.label()
            data[text] = ent
        else:
            continue
    return data

def toSentences(text):
    return nltk.sent_tokenize(text)

def sentiment(text):
    from nltk.sentiment import SentimentIntensityAnalyzer
    vader_analyzer = SentimentIntensityAnalyzer()
    return vader_analyzer.polarity_scores(text)

def stats(sentences):
    counts = list(map(lambda sentence: len(sentence), sentences))
    print("max: {}, avg: {}".format(max(counts), sum(counts)/len(counts)))

def task1PerSentece(text):
    sentences = toSentences(text)
    tokensPersentences = [nltk.word_tokenize(s) for s in sentences]

    print("Tokens:")
    stats(tokensPersentences)

    print("Strip stopwords and punctuation:")
    tokensPersentences = list(map(strip, tokensPersentences))
    stats(tokensPersentences)

    print("Lematized and stemmed tokens:")
    tokensPersentences = list(map(lambda s: list(set(stemAndLem(s))), tokensPersentences))
    stats(tokensPersentences)

    print("Tag one sentence:")
    print(tag(tokensPersentences[0]))

    print("Setniment one sentence:")
    print(sentiment(sentences[0]))

def task2(text):
    tokens = nltk.word_tokenize(text)
    print("Top tokens:")
    print(tokenCounts(tokens)[0:5])

    print("Top tokens without punctuation:")
    print(tokenCounts(stripPunctuation(tokens))[0:5])

    print("Top tokens without punctuation and stopwords:")
    print(tokenCounts(strip(tokens))[0:5])

    print("Top tokens without punctuation and stopwords after lemm and stem:")
    print(tokenCounts(stemAndLem(strip(tokens))))

    print("TOP Nouns")
    print(tokenCounts(list(filter(lambda pair: pair[1] == 'NNP', tag(strip(tokens)))))[0:5])

    print("TOP verbs")
    print(tokenCounts(list(filter(lambda pair: pair[1] == 'VBP', tag(strip(tokens)))))[0:5])

    print("Entities:")
    print(tokenCounts(extractEntities(tag(strip(tokens)))))

    print("Sentiment")
    print(sentiment(text))
    print("Most positive")
    print(sorted(toSentences(text), key=lambda x: sentiment(x)['pos'], reverse=True)[0:5])

with open('book.txt', 'r') as f:
    text = f.read()
    task1PerSentece(text)
    task2(text)

    # task3
    print("Tokens:")
    print(len(strip(nltk.word_tokenize(text))))
    tokens = strip(nltk.word_tokenize(text))
    print("Unusual tokens:")
    print(len(set(tokens) - set(brown.words())))
    print(set(tokens) - set(brown.words()))


    # print(tag(strip(tokens)))
    # tokens = nltk.word_tokenize(text)
    # tagged = nltk.pos_tag(tokens)
    # print(extractEntities(tag(strip(tokens))))