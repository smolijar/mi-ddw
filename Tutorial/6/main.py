from math import sqrt
from numpy import genfromtxt
import numpy as np
from numpy import dot
from numpy.linalg import norm as norm


def loadData():
    data = genfromtxt('ml-latest-small/ratings.head.csv', delimiter=',', skip_header=True)
    return data

def processRating(rating):
    return [int(rating[0]), int(rating[1]), float(rating[2])]

data = [processRating(rating) for rating in loadData()]
userIds = set()
movieIds = set()
for rating in data:
    userIds.add(rating[0])
    movieIds.add(rating[1])

userIds = list(userIds)
movieIds = list(movieIds)

# initialize users
users = []
for user in userIds:
    users.append(np.empty(len(movieIds)) * np.nan)

# fill user vectors
for rating in data:
    users[userIds.index(rating[0])][movieIds.index(rating[1])] = rating[2]

# def user_sim_cosine_sim(person1, person2):
#     a = []
#     b = []
#
#     return dot(person1, person2) / (norm(person1) * norm(person2))

def mean(vec):
    return sum(vec) / len([x for x in vec if x != 0])

def user_sim_pearson_corr(person1, person2):
    A = 0
    B1 = 0
    B2 = 0
    for i in range(len(person1)):
        if(person1[i] * person2[i] != 0):
            A = A + (person1[i] - mean(person1)) * (person2[i] - mean(person2))
            B1 = B1 + pow((person1[i] - mean(person1)), 2)
            B2 = B2 + pow((person2[i] - mean(person2)), 2)
    return A / (sqrt(B1) * sqrt(B2))

def most_similar_users(person, k, persons):
    return [
               x['id'] for x in
               sorted(persons, key = lambda a: (-user_sim_pearson_corr(a['ratings'], person['ratings'])))
               if x['id'] != person['id']
           ][0:k]

# def user_recommendations(person, persons):
#     similiarUsers = most_similar_users(person, 5, persons)
#     assumedReccomendations = [0 for x in person]
#     for userId in similiarUsers:
#         assumedReccomendations[]

data = genfromtxt('small-dataset.csv', delimiter=',', dtype=int)
data = [{'ratings': rating, 'id': i} for i, rating in enumerate(data)]
for user in data:
    print(user)
    print('mean:', mean(user['ratings']))
    # print('sim to 3:', user_sim_cosine_sim(user['ratings'], data[2]['ratings']))
    print('pearson to 3:', user_sim_pearson_corr(user['ratings'], data[2]['ratings']))
    print('top 2:', most_similar_users(user, 2, data))
    # print('top 2:', user_recommendations(user, data))
    print('----')
