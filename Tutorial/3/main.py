import networkx as nx
import matplotlib.pyplot as plt
from networkx.drawing.nx_agraph import graphviz_layout
import nltk

def stripPunctuation(tokens):
    from string import punctuation
    return [token for token in tokens if token not in punctuation]
def stripStopwords(tokens):
    from nltk.corpus import stopwords
    return [token for token in tokens if token not in stopwords.words('english')]
def strip(tokens):
    return stripStopwords(stripPunctuation(tokens))
def extractEntities(tagged):
    ne_chunked = nltk.ne_chunk(tagged, binary=False)
    data = {}
    for entity in ne_chunked:
        if isinstance(entity, nltk.tree.Tree):
            text = " ".join([word for word, tag in entity.leaves()])
            ent = entity.label()
            data[text] = ent
        else:
            continue
    return data

def tag(tokens):
    return nltk.pos_tag(tokens)

text = None
with open('cs.txt', 'r') as f:
    text = f.read()

entitiesPerSentence = [[t for t in extractEntities(tag(strip(nltk.word_tokenize(sentence))))] for sentence in nltk.sent_tokenize(text)]
print(entitiesPerSentence)
entityRelations = entitiesPerSentence
edges = []
wedges = []
print(entityRelations)
for rel in entityRelations:
    for e1 in rel:
        for e2 in rel:
            if(e1 != e2):
                edges.append((e1, e2))

from collections import Counter
for k, v in Counter(edges).items():
    wedges.append((k[0], k[1], v))
print(wedges)
G=nx.Graph()
G.add_weighted_edges_from(wedges)


print("Nodes", len(G.nodes()))
print("Edges", len(G.edges()))
componentlens = []
for component in nx.connected_components(G):
    componentlens.append(len(component))
print("Component sizes", componentlens)

pos = graphviz_layout(G, prog="fdp")


# CENTRALITIES
centralities = [nx.degree_centrality, nx.closeness_centrality,
 nx.betweenness_centrality, nx.eigenvector_centrality]
region=220
for centrality in centralities:
    region+=1
    plt.subplot(region)
    plt.title(centrality.__name__)
    nx.draw(G, pos, labels={v:str(v) for v in G},
      cmap = plt.get_cmap("bwr"), node_color=[centrality(G)[k] for k in centrality(G)])
plt.savefig("centralities.png")
plt.show()

# COMUNTITIES
communities = {node: cid + 1 for cid, community in enumerate(nx.k_clique_communities(G, 3)) for node in community}
pos = graphviz_layout(G, prog="fdp")
nx.draw(G, pos,
        labels={v: str(v) for v in G},
        cmap=plt.get_cmap("rainbow"),
        node_color=[communities[v] if v in communities else 0 for v in G])
plt.savefig("communities.png")
plt.show()

# ALL ENTITIES RELATIONS
plt.figure(figsize=(20,10))
nx.draw(G, pos,
        labels={v:str(v) for v in G},
        cmap = plt.get_cmap("bwr"),
        node_size=[1000*G.degree(v) for v in G],
        node_color=[G.degree(v) for v in G],
        font_size=12
       )
plt.savefig("relations.png")
nx.write_gexf(G, "export.gexf")
plt.show()