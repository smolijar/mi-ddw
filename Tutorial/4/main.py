import numpy as np
import networkx as nx
import matplotlib.pyplot as plt
from networkx.drawing.nx_agraph import graphviz_layout

def drawGraph(G, filename):
    pos = graphviz_layout(G, prog="fdp")
    nx.draw(G, pos,
            labels={v: str(v) for v in G},
            cmap=plt.get_cmap("bwr"),
            node_size=[1000 * G.degree(v) for v in G],
            node_color=[G.degree(v) for v in G],
            font_size=12
            )
    plt.show()
    plt.savefig(filename + ".png")

def readInput(inputFile, alpha):
    text = None
    with open(inputFile, 'r') as f:
        pages = int(f.readline().strip())
        graph = nx.DiGraph()
        edges = []
        H = np.zeros((pages, pages))
        graph.add_weighted_edges_from(edges)
        for pageFrom, line in enumerate(f.readlines()):
            for edge in line.strip().split():
                [pageTo, links] = [int(x) for x in edge.split(':')]
                edges.append([pageFrom, pageTo, links])
                H[pageFrom][pageTo] = links
        graph.add_weighted_edges_from(edges)
        S = np.copy(H)
        # scale
        for idx, row in enumerate(H):
            s = row.sum()
            if(s == 0):
                H[idx] = [0 for c in row]
                S[idx] = [1 / pages for c in row]
            else:
                H[idx] = [c/s for c in row]
                S[idx] = row
        e = np.zeros((pages, pages))
        e.fill(1)
        G = alpha*S + (1 - alpha)*(1 / pages)*e
        # drawGraph(graph, inputFile)
        return [H, S, G]

def compute(filename, iterations, alpha):
    print(filename)
    print('iters:', iterations)
    [H,S,G] = [np.matrix(m) for m in readInput(filename, alpha)]
    # print(H)
    # print(S)
    # print(G)
    pages = len(S)
    pih = np.array([1 / pages for i in range(pages)])
    pis = np.array([1 / pages for i in range(pages)])
    pig = np.array([1 / pages for i in range(pages)])

    for i in range(iterations):
        pih = pih * H
        pis = pis * S
        pig = pig * G

    print([round(x,3) for x in pih.tolist()[0]])
    print([round(x,3) for x in pis.tolist()[0]])
    print([round(x,3) for x in pig.tolist()[0]])
    print()

for i in range(1,5):
    compute('data/test' + str(i) + '.txt', 16, 0.9)

# compute('data/test3.txt', 16, 0.9)