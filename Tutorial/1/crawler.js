const Crawler = require('crawler');
const fs = require('fs');
const url = require('url');
const people = [];
const urls = [];
let depth = 100;

function createSitemap() {
    let sitemap = `<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd">`;
    urls.forEach((url) => {
        sitemap += `<url>
            <loc>${url.url}</loc>
            <lastmod>${url.lastMod}</lastmod>
            <changefreq>always</changefreq>
            <priority>1</priority>
        </url>`;
    });
    sitemap += `</urlset>`;
    return sitemap;
}

function processResults() {
    fs.writeFile(`${__dirname}/sitemap.xml`, createSitemap());
    fs.writeFile(`${__dirname}/people.json`, JSON.stringify(people));
}

const callback = function (error, res, done) {
    if (error) {
        console.log(error);
    } else {
        const $ = res.$;
        console.log(`@${res.request.uri.href}`);
        $('.person').each((i, personTag) => {
            const person = {};
            $(personTag).find('span').each((i, attrTag) => {
                person[$(attrTag).attr('class')] = $(attrTag).text();
            });
            people.push(person);
            console.log(`   P${JSON.stringify(person)}`);
        });

        if (depth-- < 1) {
            processResults();
            return;
        }

        $('a').each((i, elem) => {
            const href = 'http://localhost:8000' + $(elem).attr('href');
            urls.push({url: href, lastMod: new Date()});
            crawler.queue(href);
            console.log(`   q>${href}`);
        });
    }
    done();
};

const crawler = new Crawler({
    maxConnections: 1,
    rateLimit: 1100,
    userAgent: 'DDW',
    skipDuplicates: true,
    callback: callback
});

crawler.queue({
    uri: 'http://localhost:8000',
});