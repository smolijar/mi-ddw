import csv
from collections import namedtuple
from collections import Counter
import math




Click = namedtuple('Click', 'localID visitID pageName catName extCatName topicName timeOnPage') #pageID visitID pageName catName catID extCatName extCatID topicName topicID timeOnPage pageScore
Visitor = namedtuple('Visitor', 'visitID lengthSeconds refererType')


def readVisitors():
    visitors = []
    treshold = 1
    referers = {}
    with open('wum_dataset_hw/search_engine_map.csv', 'r', encoding='utf-8') as csvfile:
        spamreader = csv.reader(csvfile, delimiter=',', quotechar='"')
        next(spamreader, None)
        for row in spamreader:
            referers[row[0]] = row[1]

    with open('wum_dataset_hw/visitors.csv', 'r', encoding='utf-8') as csvfile:
        spamreader = csv.reader(csvfile, delimiter=',', quotechar='"')
        next(spamreader, None)
        for row in spamreader:
            visitor = Visitor(
                visitID=int(row[0]),
                refererType=referers[row[1]],
                lengthSeconds=int(row[4]),
            )
            visitors.append(visitor)
    filteredVisitors = [v for v in visitors if v.lengthSeconds > treshold]
    print('# VISITORS')
    print('Skipping {}% of the visitors who spend less then {} second(s) on the site.'
        .format(
            round((1-len(filteredVisitors)/len(visitors))*100),
            treshold
        )
    )
    print('Reduced {} visitors to {}.'.format(len(visitors), len(filteredVisitors)))
    return filteredVisitors



def readClicks(visitors, conversions):
    clicks = []
    sidConversions = {c:[] for c in conversions}
    totalClicks = 0
    with open('wum_dataset_hw/clicks.csv', 'r', encoding='utf-8') as csvfile:
        spamreader = csv.reader(csvfile, delimiter=',', quotechar='"')
        next(spamreader, None)
        visitorsIds = set([v.visitID for v in visitors])
        for row in spamreader:
            totalClicks = totalClicks + 1
            click = Click(
                localID=int(row[0]),
                visitID=int(row[2]),
                timeOnPage=int(row[10]),
                pageName=row[3],
                catName=row[4],
                extCatName=row[6],
                topicName=row[8]
            )

            if(click.visitID in visitorsIds):
                if(click.pageName in (microConversion + mainConversion)):
                    sidConversions[click.pageName].append(click.visitID)
                clicks.append(click)

    print('\n# CLICKS')
    print('Accepting only valid visitors we reduced {} clicks to {}.'.format(totalClicks, len(clicks)))

    print('Conversions:')
    for conversion, sids in sidConversions.items():
        print(' - {}: {}'.format(conversion, len(sids)))
    return [clicks, sidConversions]

def findChains(clicks, sidConversions):
    ruleChains = {c:[] for c in sidConversions.keys()}
    for conversion, sids in sidConversions.items():
        for sid in sids:
            relevantClicks = [click for click in clicks if click.visitID == sid]
            chain = []
            for i, click in enumerate(relevantClicks):
                chain.append(click.pageName)
                if(click.pageName == conversion):
                    break
            if (len(chain) > 0):
                ruleChains[conversion].append(chain)

    print('\n# PAGE CHAINS')
    for conversion, chains in ruleChains.items():
        print('## {} [{} chains]'.format(conversion, len(chains)))
        for chain in chains[0:3]:
            print(' - ', chain)
        print(' - ', '...')

    return ruleChains

def conversionRuleAsociation(chain, support, minConfidence, conversion):
    def frequentItems(transactions, support):
        counter = Counter()
        for trans in transactions:
            counter.update(frozenset([t]) for t in trans)
        return set(item for item in counter if counter[item] / len(transactions) >= support), counter

    def filterCandidates(transactions, itemsets, support):
        counter = Counter()
        for trans in transactions:
            subsets = [itemset for itemset in itemsets if itemset.issubset(trans)]
            counter.update(subsets)
        return set(item for item in counter if counter[item] / len(transactions) >= support), counter

    def generateCandidates(L, k):
        candidates = set()
        for a in L:
            for b in L:
                union = a | b
                if len(union) == k and a != b:
                    candidates.add(union)
        return candidates

    def apriori(transactions, support):
        result = list()
        resultc = Counter()
        candidates, counter = frequentItems(transactions, support)
        result += candidates
        resultc += counter
        k = 2
        while candidates:
            candidates = generateCandidates(candidates, k)
            candidates, counter = filterCandidates(transactions, candidates, support)
            result += candidates
            resultc += counter
            k += 1
        resultc = {item: (resultc[item] / len(transactions)) for item in resultc}
        return result, resultc

    def genereateRules(frequentItemsets, supports, minConfidence, conversion):
        rules = []
        for itemSet in frequentItemsets:
            if (len(itemSet) > 1):
                for item in itemSet:
                    lhs = set(itemSet)
                    lhs.remove(item)
                    lhssup, unionsup, rhssup = supports[frozenset(lhs)], supports[itemSet], supports[frozenset([item])]
                    conf = unionsup / lhssup
                    rule = str(lhs) + " => " + str(item)
                    if (conf >= minConfidence and item == conversion):
                        rules.append([rule, {
                            'confidence': conf,
                            'lift': unionsup / (lhssup * rhssup),
                            'conviction': math.inf if conf == 1 else (1 - rhssup) / (1 - conf),
                        }])
        for r in sorted(rules, key=lambda x: x[1]['confidence'], reverse=True):
            print(' - ', round(r[1]['confidence'], 2), r[0], '\t\t\t', r[1])

    frequentItemsets, supports = apriori(chain, support)
    genereateRules(frequentItemsets, supports, minConfidence, conversion)
def conversionRuleAnalysis(ruleChains, support, confidence):
    print('\n# RULES')
    for conversion, chains in ruleChains.items():
        print('## {}'.format(conversion))
        conversionRuleAsociation(chains, support, confidence, conversion)


visitors = readVisitors()
mainConversion = ['APPLICATION', 'CATALOG']
microConversion = ['DISCOUNT', 'HOWTOJOIN', 'INSURANCE', 'WHOWEARE']
[clicks, sidConversions] = readClicks(visitors, mainConversion + microConversion)

ruleChains = findChains(clicks, sidConversions)

conversionRuleAnalysis(ruleChains, 0.4, 0.5)

print('\n# REFERER ANALYSIS')
for type in set([v.refererType for v in visitors]):
    print('## {}'.format(type))
    allClicks = 0
    mainCoversionClicks = 0
    microConversionClicks = 0
    for visitor in [v for v in visitors if v.refererType == type]:
        visitorsClicks = [click for click in clicks if click.visitID == visitor.visitID]
        allClicks = allClicks + len(visitorsClicks)
        mainCoversionClicks = mainCoversionClicks + len([click for click in visitorsClicks if click.pageName in mainConversion])
        microConversionClicks = microConversionClicks + len([click for click in visitorsClicks if click.pageName in microConversion])
    print(' - All clicks:\t', allClicks)
    print(' - Main conversions:\t', mainCoversionClicks)
    print(' - Micro conversions:\t',microConversionClicks)
