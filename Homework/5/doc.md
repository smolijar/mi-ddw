## Task

- Use an existing dataset from a travel agency [wum_dataset_hw.zip](https://edux.fit.cvut.cz/courses/MI-DDW.16/_media/hw/05/wum_dataset_hw.zip)
  - clicks.csv
    - LocalID - local identifier of an event
    - PageID - identifier of a visited page
    - VisitID - session identifier
    - PageName - page label
    - CatName, CatID - page type (Navigation), general information
    - ExtCat,ExtCatID - page type (Content), more specific information
    - TopicName, TopicID - topic
    - TimeOnPage - time spent on the page. Last page of the session = 30s.
    - PageScore - weight of the page using the following heuristic: (ln(o)+1)*t
    - SequenceNumber - page order within a session
  - visitors.csv
    - VisitID - session identifier
    - Referrer - anonymized referrer
    - Day - day of the visit
    - Hour - hour of the visit
    - Length_seconds - visit length in seconds
    - Length_pagecount - visit length as number of visited pages
  - search_engine_map.csv
    - Referrer - anonymized referrer
    - Type - type of the referrer domain
- Tasks
  - Execute data preprocessing
    - Design a suitable data representation for the analysis - association rule mining + any other analysis of your choice
      - e.g. file where each row represents one user visit/session and columns including all interesting descriptions or summaries of the visit
        - user-transactions matrix, pageview-feature matrix, transaction-feature matrix
    - Remove too short visits
    - Implement any other data cleaning mechanism
    - Identify conversions in data
      - main conversions
        - APPLICATION (reservation of the trip) or CATALOG (request the printed catalog) in the PageName (or category) attribute of clicks
      - micro conversions
        - DISCOUNT, HOWTOJOIN, INSURANCE, or WHOWEARE in the PageName attribute of clicks
  - Implement pattern extraction
    - Identify interesting association rules in the data (e.g. conversions in the consequent)
    - Realize any other analysis of the data of your choice (e.g. users, visits clustering etc.)

## Solution

### Implementation

Implementation available at [gitlab](https://gitlab.fit.cvut.cz/smolijar/mi-ddw/tree/master/Homework/4).

### General statistics about the dataset and preprocessing

Preprocessing was based only on

* finding visitors in `visitiors.csv` that had zero time spend on the site. This filter removed almost 2/3 of all visitors.
* Dropping some columns (topics, ...)

Here is processing and dataset output of the program:

```
# VISITORS
Skipping 64% of the visitors who spend less then 1 second(s) on the site.
Reduced 15559 visitors to 5567.

# CLICKS
Accepting only valid visitors we reduced 38451 clicks to 27041.
Conversions:
 - DISCOUNT: 202
 - INSURANCE: 22
 - HOWTOJOIN: 9
 - APPLICATION: 18
 - WHOWEARE: 209
 - CATALOG: 479
```

### Perform the association rule task

#### Data selection

Before starting the algorithm I had to extract matching co-occuring chains. In tutorial we just took rows of table data, but here we want to observe associations from the session perspective, which steps lead to conversions.

Finding CatName too abstract I ended up using the PageName as the chain nodes, representing chain of page names visited per session, selecting only those that ended with a conversion.

Here is a chain preview of the page names for each conversion:

```
# PAGE CHAINS
## INSURANCE [22 chains]
 -  ['mountain expedition', 'expedition', 'mountain expedition', 'INSURANCE']
 -  ['TravelAgency', 'WHOWEARE', 'WHOWEARE', 'Sicily Islands inner fire (Sicily Lipari)', 'INSURANCE']
 -  ['TravelAgency', 'TravelAgency', 'tours with tents', 'lastminute', 'hiking', 'accommodation with private transportation', 'lastminute', 'tours with tents', 'expedition', 'light hiking', 'lastminute', 'WHOWEARE', 'WHOWEARE', 'Greek Antiquity sea and mountains', 'Greek national parks and sea greece', 'Greece and Bulgaria stay with tours', 'ancient monuments of Greece staying in Tolu', 'Greek Antiquity sea and mountains', 'Greece and Bulgaria stay with tours', 'Greek Antiquity sea and mountains', 'France provence natural parks', 'Montenegro national parks and mountains Black Sea vacation 05', 'France French volcanoes of Auvergne', 'Greek Antiquity sea and mountains', 'Aeolian Islands', 'Romania over the mountains and monasteries in Transylvania Dracula', 'INSURANCE']
 -  ...
## HOWTOJOIN [9 chains]
 -  ['TravelAgency', 'Corsica', 'Bulgaria', 'sightseeing tours', 'Bulgaria stay with holiday trips 05', 'Italy Gargano and Apulia Caribbean Mediterranean', 'Greece and Bulgaria stay with tours', 'Corsica Corsica with light tourism holiday 05', 'HOWTOJOIN']
 -  ['light hiking', 'HOWTOJOIN']
 -  ['HOWTOJOIN']
 -  ...
## CATALOG [479 chains]
 -  ['TravelAgency', 'Far tours', 'expedition', 'n news Trips', 'n news Trips', 'tours and holiday comes into hotel', 'CATALOG']
 -  ['TravelAgency', 'CATALOG']
 -  ['TravelAgency', 'tours with tents', 'National parks and thermal Hungary and Slovakia', 'CATALOG']
 -  ...
## DISCOUNT [202 chains]
 -  ['TravelAgency', 'TravelAgency', 'TravelAgency', 'lastminute', 'Aeolian Islands', 'tours and holiday comes into hotel', 'DISCOUNT']
 -  ['TravelAgency', 'TravelAgency', 'TravelAgency', 'lastminute', 'Aeolian Islands', 'tours and holiday comes into hotel', 'DISCOUNT']
 -  ['TravelAgency', 'Corsica', 'Bulgaria', 'sightseeing tours', 'Bulgaria stay with holiday trips 05', 'Italy Gargano and Apulia Caribbean Mediterranean', 'Greece and Bulgaria stay with tours', 'Corsica Corsica with light tourism holiday 05', 'HOWTOJOIN', 'Bulgarian national parks and the sea Bulgaria holiday 05', 'DISCOUNT']
 -  ...
## WHOWEARE [209 chains]
 -  ['TravelAgency', 'stays with trips', 'WHOWEARE']
 -  ['TravelAgency', 'WHOWEARE']
 -  ['TravelAgency', 'TravelAgency', 'WHOWEARE']
 -  ...
## APPLICATION [18 chains]
 -  ['Iceland southern Norway Faroe Islands', 'island country of fire and ice', 'APPLICATION']
 -  ['Far tours', 'APPLICATION']
 -  ['Far tours', 'APPLICATION']
 -  ...
```

#### Association rules

I used ready codes from the tutorial 5 to perform asociation rules mining using apriori algorithm. The only change I made was filtering only the rules, that include given conversion goal on right hand side and only it.

Inputs are minSupport and minConfidence.

As we can see in the following program output, rules are very thin, even using relatively low bonds (minSupport = 0.4, minConfidence = 0.5) and fail to cover single conversion.

```
# RULES
## INSURANCE
 -  1.0 {'TravelAgency', 'WHOWEARE'} => INSURANCE 			 {'confidence': 1.0, 'lift': 1.0, 'conviction': inf}
 -  0.85 {'WHOWEARE'} => INSURANCE 			 {'confidence': 0.8461538461538461, 'lift': 0.8461538461538461, 'conviction': 0.0}
## HOWTOJOIN
## CATALOG
 -  0.72 {'TravelAgency'} => CATALOG 			 {'confidence': 0.7157464212678937, 'lift': 0.7157464212678937, 'conviction': 0.0}
## DISCOUNT
 -  0.72 {'TravelAgency'} => DISCOUNT 			 {'confidence': 0.7203791469194313, 'lift': 0.7203791469194313, 'conviction': 0.0}
## WHOWEARE
 -  0.61 {'TravelAgency'} => WHOWEARE 			 {'confidence': 0.6063829787234043, 'lift': 0.6063829787234043, 'conviction': 0.0}
## APPLICATION
 -  0.75 {'TravelAgency'} => APPLICATION 			 {'confidence': 0.75, 'lift': 0.75, 'conviction': 0.0}
```

### Custom analysis

For custom analysis I observed user origins from anonymized referrers and their share in all traffic and conversions respectively.

Here is program output.

```
# REFERER ANALYSIS
## NA
 - All clicks:	 13281
 - Main conversions:	 241
 - Micro conversions:	 278
## Fulltext
 - All clicks:	 8052
 - Main conversions:	 146
 - Micro conversions:	 108
## OwnWebs
 - All clicks:	 1024
 - Main conversions:	 14
 - Micro conversions:	 15
## Other
 - All clicks:	 1014
 - Main conversions:	 12
 - Micro conversions:	 12
## Partners
 - All clicks:	 1422
 - Main conversions:	 35
 - Micro conversions:	 14
## Catalogue
 - All clicks:	 2248
 - Main conversions:	 49
 - Micro conversions:	 15
```

### Comments

Looking back on the result of the association rules analysis I am disappointed. The results are weak and almost all rules follow the same scheme. Perhaps I chose unfortunate attribute for the rules at the first place.

Not sure how much was filtering the zero-time-visitors -- It was a slew of visitors but very few of the clicks. Perhaps the main effect it had on the referal anaysis.

### Issues

At first I wanted to create a funnel diagram for each conversion, but sadly the most common pages of all sessions, regarding given conversion was the conversion itself, rendering the data useless. I ended up finding the referals, which is, in my opition, interesting data.