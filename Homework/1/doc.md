# Data Acquisition - Web Crawler/Scraper

## Task

- Select a web source of your own choice for the non-trivial web crawling task.
  - The resource should contain hundreds/thousands of unique pages to crawl.
  - Each page should contain at least:
    - Title - e.g. an article title, a product title, …
    - Main content/text - a main text of the article, a description of the product, …
    - Additional features describing the page - information about author, date of publishing, product item parameters, …
- Identify possible issues with crawling:
  - Explore the robot exclusion protocol, availability of the sitemaps description, …
  - Identify issues with extraction of relevant information
    - Extraction using machine readable annotations, own set of rules/selectors, automatic detection of the content, …
- Properly design and implement the extraction task
  - Inputs and outputs of the task
  - Dealing with policies
  - Selection of the language/tools
- Configure the crawler
  - focus on crawling of just one single host (domain)
  - set the crawl interval! Otherwise you can be banned!
  - set the crawl depth
  - user-agent string
  - seed URLs
  - and other settings you consider important.

## Solution

### Motivation

For a minor project I am working on I need natural languange word set focused on given topic. The goal is to extract a large text about content regarding seed topic. I decided to implement the crawler to handle this task.

### Resource

Given the desired output I chose to work on an encyclopedia and crawl linked topics, creating a web of similiar or related contents, starting of any given article.

For large free content I chose to use [**wikipedia.org**](https://en.wikipedia.org).

### Possible issues with crawling

The crawler will need to

1. respect the robots.txt file
2. limit crawl interval
3. avoid circular reference

### Design of the extraction task

#### Inputs

1. Url to **seed** article
2. Crawling **depth**
3. Max **branching** factor

#### Outputs

1. Each article as plain text of content only text in seperate files, named after article name
2. ​JSON summary of downloaded articles containing modified date

#### Usage

`node crawler.js <https wikipedia article seed url> <depth> <max branching>`

e.g. `nodejs crawler.js https://en.wikipedia.org/wiki/Computer_science 2 10`

### Implementation

Implemented in Node.js using [node-crawler](https://github.com/bda-research/node-crawler) (it uses [cheerio](https://github.com/cheeriojs/cheerio) for scraping)

**Source codes are available at [FIT gitlab](https://gitlab.fit.cvut.cz/smolijar/mi-ddw/tree/master/Homework/1).**

### Storing data

#### Files

For the purpose of using the data later, it is convinient to store text in simple text files, for easy shell processing. Each article is thus stored in seperate file in `/wiki/` subdirectory.

#### JSON

To satisfy task requirements, article data is also stored in summary file (`summary.json`). Summary contains truncated text.

### Comment

#### Solving link filtering

Links linking to article content had to be filtered. Simple but trial-error tested decision making lead to following rules

1. Take only links in main article content (cut off banners, administration etc.)
2. Take only links beginning with `/wiki`, most of related article links are relative and with this prefix
3. Skip links containing `:`. This allows to skip *special* links, to files, hubs etc. e.g.
   1. https://en.wikipedia.org/wiki/File:Nuvola_apps_kalzium.svg
   2. https://en.wikipedia.org/wiki/Portal:Science
   3. https://en.wikipedia.org/wiki/Category:Scientific_theories

#### Date extraction

~~Alas there is no machine readable last modified format.~~ This is what I thoght at first, because it is nowhere in HTML content. It turns out, the answer is in HTTP header, so this problem was easily solved and date extraction works well on all languages now!

(Before I was scraping date from human readable text at the bottom of the page, which I tested on English and Czech wikis)

### Example

#### Input

`nodejs crawler.js https://en.wikipedia.org/wiki/Computer_science 1 3`

#### Output

##### crawler output

```
@https://en.wikipedia.org/wiki/Computer_science
   q>https://en.wikipedia.org/wiki/Science
   q>https://en.wikipedia.org/wiki/Formal_science
   q>https://en.wikipedia.org/wiki/Logic
@https://en.wikipedia.org/wiki/Science
   q>https://en.wikipedia.org/wiki/Science_(disambiguation)
   q>https://en.wikipedia.org/wiki/Formal_science
   q>https://en.wikipedia.org/wiki/Logic
@https://en.wikipedia.org/wiki/Formal_science
   q>https://en.wikipedia.org/wiki/Science
   q>https://en.wikipedia.org/wiki/Logic
   q>https://en.wikipedia.org/wiki/Mathematics
@https://en.wikipedia.org/wiki/Logic
@https://en.wikipedia.org/wiki/Science_(disambiguation)
@https://en.wikipedia.org/wiki/Mathematics
```

##### summary.json

```json
[
  {
    "title": "Computer science",
    "date": "Fri Mar 10 2017",
    "filename": "Computer-science.txt",
    "text": "Computer science is the study of the theory, experimentation, and engineering that form the basis fo..."
  },
  {
    "title": "Science",
    "date": "Mon Mar 06 2017",
    "filename": "Science.txt",
    "text": "Science[nb 1][1]:58[2] is a systematic enterprise that builds and organizes knowledge in the form of..."
  },
  {
    "title": "Formal science",
    "date": "Fri Feb 24 2017",
    "filename": "Formal-science.txt",
    "text": "Formal sciences are disciplines concerned with formal systems, such as logic, mathematics, statistic..."
  },
  {
    "title": "Logic",
    "date": "Fri Mar 10 2017",
    "filename": "Logic.txt",
    "text": "Logic (from the Ancient Greek: λογική, logikḗ[1]), originally meaning \"the word\" or \"what is spoken\"..."
  },
  {
    "title": "Science (disambiguation)",
    "date": "Tue Feb 21 2017",
    "filename": "Science-(disambiguation).txt",
    "text": "Science (from the Latin scientia, meaning \"knowledge\") usually describes the effort to understand ho..."
  },
  {
    "title": "Mathematics",
    "date": "Fri Mar 10 2017",
    "filename": "Mathematics.txt",
    "text": "Mathematics (from Greek μάθημα máthēma, “knowledge, study, learning”) is the study of topics such as..."
  }
]
```

##### Files

```sh
$ wc wiki/*                                                           
    41   3375  23825 wiki/Computer-science.txt
    11    648   4455 wiki/Formal-science.txt
   277   6134  40822 wiki/Logic.txt
    50   4785  32795 wiki/Mathematics.txt
     6    163   1056 wiki/Science-(disambiguation).txt
    72   6865  45832 wiki/Science.txt
   457  21970 148785 total
```

