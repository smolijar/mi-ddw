const Crawler = require('crawler');
const fs = require('fs');
const url = require('url');
const https = require('https');

const config = {
    disallowedPaths: [],
    userAgent: 'DDW',
    meta: []
};

function callback(error, res, done) {
    if (error) {
        console.log(error);
    } else {
        const $ = res.$;
        console.log(`@${res.request.uri.href}`);
        const title = $('#firstHeading').text();
        const modDate = new Date(res.headers['last-modified']).toDateString();
        const filename = `${title.replace(/[^\w\(\)\.@:-]/gi, '-')}.txt`;
        const content = $('#mw-content-text');
        text = '';
        content.find('p').each((i, e) => {
            text += `${$(e).text()}\n`;
        });
        fs.writeFile(`${__dirname}/wiki/${filename}`, text);
        config.meta.push({title, date: `${modDate}`, filename, text: `${text.substring(0, 100)}...`});
        if (config.depth-- > 0) {
            content.find('a').each((i, elem) => {
                if(i > config.branching) {
                    return false;
                }
                const href = $(elem).attr('href');
                // only wiki articles and skip special pages (images, books, references, all contain :)
                if(href.startsWith('/wiki/') && href.indexOf(':') === -1) {
                    if(config.disallowedPaths.includes(href)) {
                        return true;
                    }
                    const url = `${res.request.uri.protocol}//${res.request.uri.host}${href}`;
                    crawler.queue(url);
                    console.log(`   q>${url}`);
                }
            });
        }
    }
    done();
}

const crawler = new Crawler({
    maxConnections: 10,
    rateLimit: 1100,
    userAgent: config.userAgent,
    skipDuplicates: true,
    callback: callback,
}).on('drain', () => {
    fs.writeFile(`${__dirname}/summary.json`, JSON.stringify(config.meta));
});

function parseRobots(cb) {
    const response = https.get('https://en.wikipedia.org/robots.txt');
    response.on('response', (message) => {
        let data = '';
        message.on('data', (buffer) => data += buffer);
        let iCare = false;
        message.on('end', () => {
            data.split('\n').forEach((val, i) => {
                const matches = val.match(/^User-agent: (.+)$/);
                if (matches && matches[1]) {
                    iCare = matches[1] === config.userAgent || matches[1] === '*';
                }
                if (iCare && val.startsWith('Disallow')) {
                    const path = ((val.match(/^Disallow: (.*)$/) || [])[1] || '').trim();
                    if (path) {
                        config.disallowedPaths.push(path);
                    }
                }
            });
            cb(null, config.disallowedPaths);
        });
    });
}

const args = process.argv.slice(2);
if(args.length < 3) {
    console.error('node crawler.js <https wikipedia article seed url> <depth> <max branching>');
    process.exit(1);
}
else {
    config.seed = args[0];
    config.branching = args[2];
    config.depth = args[1] * config.branching;
}

parseRobots((err) => {
    if(err) {
       return console.error('Failed parsing robots.txt');
    }
    crawler.queue({
        uri: config.seed,
    });
});
