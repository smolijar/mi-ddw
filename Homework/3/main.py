import nltk

def tag(tokens):
    return nltk.pos_tag(tokens)

def freqSort(tokens):
    from collections import Counter
    counts = Counter(tokens)
    sortedCounts = sorted(counts.items(), key=lambda count:count[1], reverse=True)
    return [counter[0] for counter in sortedCounts]

def _processEntityTree(tree):
    data = []
    for entity in tree:
        if isinstance(entity, nltk.tree.Tree):
            text = " ".join([word for word, tag in entity.leaves()])
            ent = entity.label()
            data.append((text, ent))
        else:
            continue
    return data

def extractEntities(text):
    tagged = tag(nltk.word_tokenize(text))
    ne_chunked = nltk.ne_chunk(tagged, binary=False)
    data = []
    return _processEntityTree(ne_chunked)

def extractEntitiesCustom(text):
    # DT determinal
    # JJ adjective or numeral, ordinal
    # NN noun, common, singular or mass
    # NNS noun, common, plural
    # NNP noun, proper, singular
    return extractEntitiesByGrammar(text, "ENTITY: {<DT>?<JJ>*<NNP>+}")

def extractEntitiesByGrammar(text, grammar):
    tagged = tag(nltk.word_tokenize(text))
    cp = nltk.RegexpParser(grammar)
    return _processEntityTree(cp.parse(tagged))

def tagWiki(entity):
    import wikipedia
    results = wikipedia.search(entity)
    recognized = '(a thing)'
    ambiguous = False
    if(len(results) > 0):
        testWord = results[0]
        page = None
        while(True):
            try:
                page = wikipedia.page(testWord)
            except wikipedia.exceptions.DisambiguationError as e:
                ambiguous = True
                testWord = e.options[0]
                continue
            break
        txt = page.summary
        firstEntity = extractEntitiesByGrammar(txt, "ENTITY: {<VB|VBZ|VBD|VBP|VBN><DT>?<JJ>*<NNP|NNS|NN>+}")[0][0]
        recognized = ' '.join(nltk.word_tokenize(firstEntity)[1:])
        recognized += ' (ambiguous)' if ambiguous else ''
    return recognized

text = None
with open('book.txt', 'r') as f:
    text = f.read()

print('Text: Don\'t Shoot By ROBERT ZACKS')
print('sentences:', len(nltk.sent_tokenize(text)))
print()


print('## POS tagging')
print(tag(nltk.word_tokenize(text))[100:110])
print()

entity_nltk = freqSort(extractEntities(text))[:10]
entity_custom = freqSort(extractEntitiesCustom(text))[:10]

print('## NLTK Entities:')
print()
for e in entity_nltk:
    print(e[0])
    print('  nltk> ', e[1].lower())
    print('  wiki> ', tagWiki(e[0]))

print('## Custom Entities:')
print()
for e in entity_custom:
    print(e[0])
    nltke = None
    try:
        nltke = [en for en in entity_nltk if (en[0] == e[0] or en[0] in e[0] or e[0] in en[0])][0][1]
    except IndexError:
        nltke = '(not recognized as entity by nltk)'
    print('  nltk> ', nltke)
    print('  wiki> ', tagWiki(e[0]))
