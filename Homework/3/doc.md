# Text mining

## Task

- Find any suitable textual data for processing which will contain at least 500 sentences.
  - you can manually collect texts from BBC/CNN/New York Times, or
  - use the crawler from the first homework/tutorial and extend it to crawl particular website and collect content for this task, or
  - use any other suitable texts
- Perform following NLP tasks
  - POS tagging
  - NER with entity classification (using nltk.ne_chunk)
  - NER with custom patterns
    - e.g. every match of: adjective (optional) and proper noun (singular/plural) is matched as the entity
    - see [slides 31 or 38 from lecture 4](https://edux.fit.cvut.cz/courses/MI-DDW.16/_media/lectures/lecture4.pdf) for some NLTK examples using RegexpParser or custom NER
- Implement your custom entity classification
  - For each detected entity (using both nltk.ne_chunk and custom patterns)
    - Try to find a page in the Wikipedia
    - Extract the first sentence from the summary
    - Detect category from the sentence as a noun phrase
      - Example:
        - for „Wikipedia“ entity the first sentence is „Wikipedia (/ˌwɪkᵻˈpiːdiə/ or /ˌwɪkiˈpiːdiə/ WIK-i-PEE-dee-ə) is a free online encyclopedia that aims to allow anyone to edit articles.“
        - you can detect pattern “… **is**/VBZ a/DT free/JJ online/NN encyclopedia/NN …“
        - the output can be „Wikipedia“: „free online encyklopedia“
    - For unknown entities assign default category e.g. „Thing“

Wikipedia package in Python:

```python
import wikipedia
results = wikipedia.search("Wikipedia")
print(results)
page = wikipedia.page("Wikipedia")
print(page.summary)
```

## Solution

### Data source

[Don't Shoot by Robert Zacks from www.gutenberg.org](http://www.gutenberg.org/ebooks/51074)

### Implementation

**Source codes are available at [gitlab](https://gitlab.fit.cvut.cz/smolijar/mi-ddw/tree/master/Homework/3).**

### Issues

#### NER Pattern

```python
# DT determinal
# JJ adjective or numeral, ordinal
# NNP noun, proper, singular
'<DT>?<JJ>*<NNP>+'
```

#### Wiki DisambiguationError

`wikipedia.search` does not return valid page names, DisambiguationError can occur. `e.options` then contains further links, which may be valid or futher ambiguous. Empirically names are eventiually valid.

```python
testWord = results[0]
page = None
while(True):
    try:
        page = wikipedia.page(testWord)
    except wikipedia.exceptions.DisambiguationError as e:
        ambiguous = True
        testWord = e.options[0]
        continue
    break
```



### Output

Output shows

- example of POS tagging
- NLTK entities (top 10)
  - NER nltk
  - NER wiki
- Custom entities (top 10)
  - NER nltk
  - NER wiki

```
POS tagging
[('will', 'MD'), ('be', 'VB'), ('made', 'VBN'), ('ready', 'JJ'), ('for', 'IN'), ('another', 'DT'), ('unfortunate', 'NN'), ('.', '.'), ('Nevertheless', 'CC'), ('what', 'WP')]

## NLTK Entities:

Mr. Eammer
  nltk>  person
  wiki>  (a thing)
Mary
  nltk>  person
  wiki>  name (ambiguous)
Susie
  nltk>  person
  wiki>  a rap music duo (ambiguous)
Largoscope
  nltk>  person
  wiki>  (a thing)
Abominable
  nltk>  organization
  wiki>  the cryptid Bigfoot
Himalayas
  nltk>  organization
  wiki>  a mountain range
Broadway
  nltk>  gpe
  wiki>  a popular tourist attraction (ambiguous)
Hollywood
  nltk>  gpe
  wiki>  a shorthand reference
Billy Shakespeare
  nltk>  person
  wiki>  an English poet
Susie
  nltk>  gpe
  wiki>  a rap music duo (ambiguous)

## Custom Entities:

Mr. Eammer
  nltk>  PERSON
  wiki>  (a thing)
Mary
  nltk>  PERSON
  wiki>  name (ambiguous)
Susie
  nltk>  PERSON
  wiki>  a rap music duo (ambiguous)
* * * *
  nltk>  (not recognized as entity by nltk)
  wiki>  (a thing)
Largoscope
  nltk>  PERSON
  wiki>  (a thing)
Remember
  nltk>  (not recognized as entity by nltk)
  wiki>  the fourth solo studio album (ambiguous)
Okay
  nltk>  (not recognized as entity by nltk)
  wiki>  an English word
Hollywood
  nltk>  GPE
  wiki>  a shorthand reference
the Abominable Snowman
  nltk>  ORGANIZATION
  wiki>  the exploits (ambiguous)
Billy Shakespeare
  nltk>  PERSON
  wiki>  an English poet
```

### Comments on results

The implementation tackled two phases of NER. Finding the entities and categorizing them.

#### Finding entities

Theres a table of retrieved entities. Common entities are stroked through, while the diffrences are in bold.

| NLTK                  | Custom NER                 |
| --------------------- | -------------------------- |
| ~~Mr. Eammer~~        | ~~Mr. Eammer~~             |
| ~~Mary~~              | ~~Mary~~                   |
| ~~Susie~~             | ~~Susie~~                  |
| ~~Largoscope~~        | *** * * ***                |
| **Abominable**        | ~~Largoscope~~             |
| **Himalayas**         | **Remember**               |
| **Broadway**          | **Okay**                   |
| ~~Hollywood~~         | ~~Hollywood~~              |
| ~~Billy Shakespeare~~ | **the Abominable Snowman** |
| **Susie**             | ~~Billy Shakespeare~~      |

With both approaches, major characters of the scifi story have been correctly identified (Mr. Eamer, Susie, Mary). Lets take a look on the diffrences.

##### NLTK results

* Abominable is an adjective. It has probably been recognized as entity for the consistent capitalization of the first letter. The entity is incomplete. Here is the first occurance from the book:

  > for the purpose of investigating that astonishing primeval creature called '**The Abominable Snowman**,'

* Himalayas and Broadway have been correctly recognized. But both have mere 2 occurences each in the book, compared to 59 occurences of (plausibly) main character, *Mr. Eamer*.

* Susie has two occurences (not sure why)

##### Custom NER results

* *** * * *** was incorrectly recognized. It is just a delimiter of sections. Both approaches worked with unfiltered tokenized and tagget text. NLTK has recognized punctuation on its own. Strangly the stars were tagged to match the pattern, looking at the given code from tagged text: ` ... ('boxes', 'NNS'), ('.', '.'), ('*', 'JJ'), ('*', 'JJ'), ('*', 'NN'), ('*', 'NNP'), ('*', 'NNP'), ('I', 'PRP'), ('stared', 'VBD'), ...`

* Remember and Okay were incorrectly recognized, as entities for opening the sentences with capital letter, beign tagged as `NNP`. They have 3 occurences each:

  > "**Okay**, if you can't do it, then get another job. You're fired."
  >
  > "**Okay**. Turn dial number one to full force."
  >
  > "Such deep love, such clean emotion, it cuts my heart out, honestly. **Okay**, we'll give the script a scrubbing. Nobody'll put a finger on her."
  >
  > "**Remember** how we couldn't find Susie all week?" she gasped. "Well, I just found her."
  >
  > "Apparently it transmits whatever size the image is set at. **Remember** we had reduced the image of Susie and at that time you short-circuited the wires? (...)"
  >
  > But let me tell how it all started. **Remember** those awful days when television, like a monster with a wild pituitary gland, grew until it took the word 'colossal' away from filmdom?

* the Abominable Snowman was correctly recognized opposed to nltk

#### Entity recognition

| Entity                 | NLTK                               | Wiki                                     | Comment                                  | Winner |
| ---------------------- | ---------------------------------- | ---------------------------------------- | ---------------------------------------- | ------ |
| Mr. Eammer             | person                             | (a thing)                                | Mr. Eamers is main character. NLTK is closer. | NLTK   |
| Mary                   | person                             | name (ambiguous)                         | Both vague, both correct.                | -      |
| Susie                  | person                             | a rap music duo (ambiguous)              | Susie is one of main characters, not a rap duo, but a single woman. Vague wins. | NLTK   |
| Largoscope             | person                             | (a thing)                                | Largoscope si probably a device in the book. Wiki wins, byt finding nothing. | Wiki   |
| Abominable             | organization                       | the cryptid Bigfoot                      | The Abominable Snowman is a bigfoot like creature in the book. Wiki wins with lucky hand, finding unrelated movie. | Wiki   |
| Himalayas              | organization                       | a mountain range                         |                                          | Wiki   |
| Broadway               | gpe                                | a popular tourist attraction (ambiguous) |                                          | Wiki   |
| Hollywood              | gpe                                | a shorthand reference                    | Wiki article first sentence was to complicated to grasp. | NLTK   |
| Billy Shakespeare      | person                             | an English poet                          | Billy Shakespeare was mistaken for William Shakespeare. | NLTK   |
| * * * *                | (not recognized as entity by nltk) | (a thing)                                | Unurprisingly not recognized by either.  | -      |
| Remember               | (not recognized as entity by nltk) | the fourth solo studio album (ambiguous) | Word does not refer to album.            | -      |
| Okay                   | (not recognized as entity by nltk) | an English word                          | General wiki categorization but precise in given context. | Wiki   |
| the Abominable Snowman | ORGANIZATION                       | the exploits (ambiguous)                 | Both matches are terrible                | -      |

Summing up points, Wiki has 5 correct classifications and NLTK has only 4. Wiki approach was very lucky in some cases, but is probably generally not as suitable for practical use. If it finds the correct article, its definition is much more descriptive, but it is more likely to classify entity as something not even related.

This could be improved by throwing away the ambiguous results.