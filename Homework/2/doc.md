# Indexing & Document Retrieval

## Task

- Select a web source of your own choice for the non-trivial web crawling task.
  - The resource should contain hundreds/thousands of unique pages to crawl.
  - Each page should contain at least:
    - Title - e.g. an article title, a product title, …
    - Main content/text - a main text of the article, a description of the product, …
    - Additional features describing the page - information about author, date of publishing, product item parameters, …
- Identify possible issues with crawling:
  - Explore the robot exclusion protocol, availability of the sitemaps description, …
  - Identify issues with extraction of relevant information
    - Extraction using machine readable annotations, own set of rules/selectors, automatic detection of the content, …
- Properly design and implement the extraction task
  - Inputs and outputs of the task
  - Dealing with policies
  - Selection of the language/tools
- Configure the crawler
  - focus on crawling of just one single host (domain)
  - set the crawl interval! Otherwise you can be banned!
  - set the crawl depth
  - user-agent string
  - seed URLs
  - and other settings you consider important.

## Solution

### Results

I was actually surprised how bad the results where. Not sure if its a really hard dataset or poor implementation. Having no practical expirience with document retrieval I would expect hte precision to go over 70% with TF-IDF

#### Models

TF-IDF model was expected to provide best results and it has. Suprising fact is that in the results, the bool model was actually better than the pure TF one. Not sure if its really tha case, or I made an error along the way. Other results follow the expectations.

#### Metric

As expected, euclidian distance performs worse compared to cosine similiarty given the quality attribues precision recall. Rather unexpected for me is the jump in gaps in TF-IDF and other models -- whereas in Boolean and TF model, euclidian distance performs worse by order (almost precicely 10 times on both models), on TF-IDF model it is only sligtly behind CosineSimiliarity (by 0.2)

### Comments

I was not sure how to use the sklearn library for TF and bool models and had a hard time googling. I finally managed by browsing the library source and reading the official docs. Apperenty few people need just TF or boolean model.

First off I tried to implement trivials (bool model and euclidian distance) by myself, but I ralized sklearn uses itself tokenization and stopwords, making it really hard to compare the results.

Ended up using sklearn for all requirements, hopefully correctly.

### Implementation

**Source codes are available at [FIT gitlab](https://gitlab.fit.cvut.cz/smolijar/mi-ddw/tree/master/Homework/2).**

### Output

Also available in csv in [repository](https://gitlab.fit.cvut.cz/smolijar/mi-ddw/blob/master/Homework/2/results.csv).

```
CosineSim
Binary
precision: 0.14888888888888893
recall: 0.2214018242118986
fmeasure: 0.16497864172485052
Tf
precision: 0.128888888888889
recall: 0.17788354266755044
fmeasure: 0.1393823814801127
TfIdf
precision: 0.24088888888888904
recall: 0.34649102697699563
fmeasure: 0.2634016385242179

Euclidian distance
Binary
precision: 0.015111111111111117
recall: 0.021537736422793895
fmeasure: 0.016781906554249174
Tf
precision: 0.012000000000000005
recall: 0.01591029851374679
fmeasure: 0.012598885808308343
TfIdf
precision: 0.22000000000000017
recall: 0.32034911768802754
fmeasure: 0.2416507547578625
```