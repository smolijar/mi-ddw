# import
from sklearn.metrics.pairwise import cosine_similarity
from sklearn.metrics.pairwise import euclidean_distances
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.feature_extraction.text import CountVectorizer
import numpy as np

# prepare corpus
corpus = []
results = []
cosineStats = {'tf':[], 'tfidf':[], 'binary':[]}
euclidianStats = {'tf':[], 'tfidf':[], 'binary':[]}
DOCUMENTS = 1400
QUERIES = 225

# read docs and queries to corpus, reference to results
for d in range(DOCUMENTS):
    f = open("./cranfield/d/" + str(d + 1) + ".txt")
    corpus.append(f.read())
for qn in range(QUERIES):
    f = open("./cranfield/q/" + str(qn + 1) + ".txt")
    corpus.append(f.read())
for r in range(QUERIES):
    f = open("./cranfield/r/" + str(r + 1) + ".txt")
    results.append([int(x) for x in f.read().split('\n')[:-1]])

# prepare model matrixes
tfidf_matrix = TfidfVectorizer().fit_transform(corpus)
tf_matrix = CountVectorizer().fit_transform(corpus)
binary_matrix = CountVectorizer(binary=True).fit_transform(corpus)

# For each query, add score for different methods
for qn in range(QUERIES):
    def top_sim(fun, q, corp, distance=False, k=10):
        ''':return top k similiar documents from corp'''
        sim = np.array(fun(q, corp)[0])
        return sim.argsort()[-k:][::-1] + 1 if not distance else sim.argsort()[:k][::-1] + 1

    def quality(result, reference):
        ''':return precision, recall and fmeasure of result compared to reference'''
        tp = len(set(result).intersection(reference))
        fp = len(set(result) - set(reference))
        fn = len(set(reference) - set(result))
        # print(tp, set(result), set(reference))
        re = tp / (tp + fn)
        pr = tp / (tp + fp)
        return {'precision': pr, 'recall': re, 'fmeasure': 0 if pr == 0 or re == 0 else 2*pr*re/(pr+re)}

    # all for cosine
    top_cosim_tfidf = top_sim(cosine_similarity, tfidf_matrix[DOCUMENTS + qn], tfidf_matrix[0:DOCUMENTS])
    top_cosim_tf = top_sim(cosine_similarity, tf_matrix[DOCUMENTS + qn], tf_matrix[0:DOCUMENTS])
    top_cosim_binary = top_sim(cosine_similarity, binary_matrix[DOCUMENTS + qn], binary_matrix[0:DOCUMENTS])
    cosineStats['tfidf'].append(quality(top_cosim_tfidf, results[qn]))
    cosineStats['tf'].append(quality(top_cosim_tf, results[qn]))
    cosineStats['binary'].append(quality(top_cosim_binary, results[qn]))

    # all for l2
    top_euclidian_tfidf = top_sim(euclidean_distances, tfidf_matrix[DOCUMENTS + qn], tfidf_matrix[0:DOCUMENTS], distance=True)
    top_euclidian_tf = top_sim(euclidean_distances, tf_matrix[DOCUMENTS + qn], tf_matrix[0:DOCUMENTS], distance=True)
    top_euclidian_binary = top_sim(euclidean_distances, binary_matrix[DOCUMENTS + qn], binary_matrix[0:DOCUMENTS], distance=True)
    euclidianStats['tfidf'].append(quality(top_euclidian_tfidf, results[qn]))
    euclidianStats['tf'].append(quality(top_euclidian_tf, results[qn]))
    euclidianStats['binary'].append(quality(top_euclidian_binary, results[qn]))


# print resulted averages
def stats(x):
    mean = lambda x: sum(x) / len(x)
    print('precision:', mean([r['precision'] for r in x]))
    print('recall:', mean([r['recall'] for r in x]))
    print('fmeasure:', mean([r['fmeasure'] for r in x]))
print('CosineSim')
print('Binary')
stats(cosineStats['binary'])
print('Tf')
stats(cosineStats['tf'])
print('TfIdf')
stats(cosineStats['tfidf'])

print('\nEuclidian distance')
print('Binary')
stats(euclidianStats['binary'])
print('Tf')
stats(euclidianStats['tf'])
print('TfIdf')
stats(euclidianStats['tfidf'])