import csv
import networkx as nx
import matplotlib.pyplot as plt
from networkx.drawing.nx_agraph import graphviz_layout
import math
import operator




wedges = []
wedgesfull = []
casts = {}
with open('casts.csv', 'r', encoding='utf-8') as csvfile:
     spamreader = csv.reader(csvfile, delimiter=';', quotechar='"')
     for row in spamreader:
         movie = row[1]
         actor = row[2]
         if(actor.strip() != '' and movie.strip() != '' and actor.strip() != 's a'):
             if(movie not in casts):
                 casts[movie] = set()
             casts[movie].add(actor)

casts = {movie: list(actors) for movie, actors in casts.items()}
cocasts = []
for movie, actors in casts.items():
    for a1 in actors:
        for a2 in actors:
            if(a1 < a2):
                cocasts.append((a1, a2))

from collections import Counter
for k, v in Counter(cocasts).most_common(50):
    wedges.append((k[0], k[1], v))
for k, v in Counter(cocasts).most_common():
    wedgesfull.append((k[0], k[1], v))

G=nx.Graph()
G.add_weighted_edges_from(wedges)
Gfull=nx.Graph()
Gfull.add_weighted_edges_from(wedgesfull)

def graphStats(G):
    verts = len(G.nodes())
    edges = len(G.edges())
    print('# General statistics\n')
    print("Nodes:\t", verts)
    print("Edges:\t", edges)
    print("Density:\t", (2 * edges) / ((verts)*(verts-1)))
    nx.write_gexf(G, "export.gexf")

def kevinBaconStats(G):
    print('\n\n# Kevin Bacon\n')
    kevinBacon = []
    for n in G.nodes():
        try:
            length = nx.shortest_path_length(G, source=n, target='Kevin Bacon')
        except nx.exception.NetworkXNoPath:
            length = math.inf
        kevinBacon.append((n, length))
    kevinBaconNoInf = [actor for actor in kevinBacon if actor[1] != math.inf]
    kevinBaconNoInfLens = [actor[1] for actor in kevinBacon if actor[1] != math.inf]
    print("Kevin Bacon average:\t", sum(kevinBaconNoInfLens)/len(kevinBaconNoInfLens))
    print("Kevin Bacon max:\t", sorted(kevinBacon, key=operator.itemgetter(1), reverse=True)[:10])
    print("Kevin Bacon max no inf:\t", sorted(kevinBaconNoInf, key=operator.itemgetter(1), reverse=True)[:10])
    print("Kevin Bacon min:\t", sorted(kevinBacon, key=operator.itemgetter(1))[:10])

def graphComponents(G):
    print('\n\n# Graph components\n')

    componentlens = []
    for component in nx.connected_components(G):
        componentlens.append(len(component))
    print("Components:\t", len(componentlens))
    print("Component sizes:\t", sorted(componentlens, reverse=True))


# pos = graphviz_layout(G)
pos = graphviz_layout(G, prog="fdp")

def graphCentralities(G, pos):
    centralities = [nx.degree_centrality, nx.closeness_centrality,
     nx.betweenness_centrality, nx.eigenvector_centrality]
    print('\n\n# Centrality key players\n')
    for centrality in centralities:
        players = sorted(centrality(G).items(), key=operator.itemgetter(1), reverse=True)
        print(centrality.__name__ + ':\t', [p[0] for p in players][:10])

    def plotCentrailities(G, pos):
        for centrality in centralities:
            nx.draw(G, pos, labels={v: str(v) for v in G},
                    cmap=plt.get_cmap("bwr"), node_color=[centrality(G)[k] for k in centrality(G)])
            plt.savefig(centrality.__name__ + ".png")
            # plt.show()
            plt.close()
    plotCentrailities(G, pos)

def graphComunities(G, pos):
    print('\n\n# Comunities\n')
    def plotCommunities(G, pos):
        communities = {node: cid + 1 for cid, community in enumerate(nx.k_clique_communities(G, 3)) for node in community}
        nx.draw(G, pos,
                labels={v: str(v) for v in G},
                cmap=plt.get_cmap("rainbow"),
                node_color=[communities[v] if v in communities else 0 for v in G])
        plt.savefig("communities.png")
        # plt.show()
        plt.close()

    actorCommunity = {node: cid + 1 for cid, community in enumerate(nx.k_clique_communities(G, 3)) for node in community}
    communityNumbers = set([actor[1] for actor in actorCommunity.items()])
    for number in communityNumbers:
        print(number, [actor[0] for actor in actorCommunity.items() if actor[1] == number])
    plotCommunities(G, pos)

def graphPlot(G, pos):
    plt.figure(figsize=(20, 10))
    nx.draw(G, pos,
            labels={v: str(v) for v in G},
            cmap=plt.get_cmap("bwr"),
            node_size=[1000 * G.degree(v) for v in G],
            node_color=[G.degree(v) for v in G],
            font_size=12
            )
    plt.savefig("relations.png")
    # plt.show()

graphStats(Gfull)
graphComponents(Gfull)
graphCentralities(G, pos)
graphComunities(G, pos)
kevinBaconStats(Gfull)
graphPlot(G, pos)





