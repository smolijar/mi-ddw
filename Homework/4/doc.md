# Social Network Analysis

## Task

- Use an existing [Movie dataset](https://archive.ics.uci.edu/ml/datasets/Movie)
  - Focus on „casts“ - data that provide information about actors in movies
  - You can also use already converted data to [csv](https://edux.fit.cvut.cz/courses/MI-DDW.16/_media/hw/04/casts.zip) from [https://github.com/cernoch/movies](https://github.com/cernoch/movies)
- Convert „casts“ data to a graph
  - Create a node for each actor
  - Create an edge if two actors appeared in the same movie
- Perform Social Network Analysis
  - Compute general statistics
  - Identify key players using centrality measures
  - Identify clusters/communities in graph
  - Compute „Kevin Bacon“ number for each actor with selected key player
  - Visualise important aspects of the analysis

## Solution

### Data source

I was using only the provided csv file.

### Implementation

**Source codes are available at [gitlab](https://gitlab.fit.cvut.cz/smolijar/mi-ddw/tree/master/Homework/4).**

### Output

For the performance issues and plot readability, not all actions are performed on the full graph of casts. The limited graph is constructed only using 50 most common co-cast couples. This number is also the edge weight for both graphs.

Graph in gexf format is also available at [gitlab](https://gitlab.fit.cvut.cz/smolijar/mi-ddw/raw/master/Homework/4/export.gexf).

#### General statistics

Stats of whole graph of casts. (Actors *"s a"* removed. See issues.)

```sh
Nodes:	 16281
Edges:	 152266
Density:	 0.0011489402865853943
```

The plotted graph is limited to top 50 edges.

![Main graph of actors co-casts](relations.png)

#### Graph components

Graph component stats of the full graph.

```sh
Components:	 417
Component sizes:	 [14764, 18, 15, 15, 14, 14, 12, 12, 12, 11, 10, 10, 10, 9, 9, 9, 9, 9, 9, 8, 8, 8, 8, 8, 8, 8, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2]
```

#### Centrality key players

Centralities are computed on the smaller graph.

```sh
degree_centrality:	 ['Lois Maxwell', 'Groucho Marx', 'Harpo Marx', 'Chico Marx', 'Nigel Bruce', 'Basil Rathbone', 'Margaret Dumont', 'Zeppo Marx', 'Dennis Hoey', 'Mary Gordon']
closeness_centrality:	 ['Lois Maxwell', 'Groucho Marx', 'Harpo Marx', 'Chico Marx', 'Margaret Dumont', 'Zeppo Marx', 'Nigel Bruce', 'Basil Rathbone', 'Dennis Hoey', 'Mary Gordon']
betweenness_centrality:	 ['Lois Maxwell', 'Ann Sheridan', 'Humphrey Bogart', 'Peter Sellers', 'Errol Flynn', 'Oliver Hardy', 'Woody Allen', 'Fred Astaire', 'Groucho Marx', 'Harpo Marx']
eigenvector_centrality:	 ['Groucho Marx', 'Harpo Marx', 'Chico Marx', 'Margaret Dumont', 'Zeppo Marx', 'Nigel Bruce', 'Basil Rathbone', 'Mary Gordon', 'Dennis Hoey', 'Oliver Hardy']
```

##### Degree centraility

![](degree_centrality.png)

##### Closeness centraility

![](closeness_centrality.png)

##### Betweenness centraility

![](betweenness_centrality.png)

##### Eigenvector centraility

![](eigenvector_centrality.png)

#### Comunities

Communities are inspected only on the limited acts graph.

```sh
1 ['Margaret Dumont', 'Chico Marx', 'Zeppo Marx', 'Groucho Marx', 'Harpo Marx']
2 ['Mary Gordon', 'Dennis Hoey', 'Basil Rathbone', 'Nigel Bruce']
3 ['Bing Crosby', 'Dorothy Lamour', 'Bob Hope']
4 ['Asta', 'Myrna Loy', 'William Powell']
```

![](communities.png)

#### Kevin Bacon

As apperant from the graph components inspection, not all actors are transitively related to Kevin Bacon --- those have their Kevin Bacon coefficient set to infinity. Luckily, Mr. Bacon is in the major component.

Kevin Bacon stats were observed on the full graph.

```sh
Kevin Bacon average:	 3.1517881332972095
Kevin Bacon max:	 [('Paul Land', inf), ('Armando Boffilde Pirovano', inf), ('Susan Berman', inf), ('Michelle Sweeney', inf), ('Jenny Cole', inf), ('John Lupton', inf), ('Mechtild Grossman', inf), ('Kenneth Riegel', inf), ('Yacef Saadi', inf), ('Georges Corraface', inf)]
Kevin Bacon max no inf:	 [('Haradhan Banerjee', 7), ('Haren Chatterjee', 7), ('Kazuo Hasegawa', 7), ('Nathalie Pascoud', 6), ('Jack Wouterse', 6), ('Loes Wouterson', 6), ('Hidetaka Yoshioka', 6), ('Jose Sacristan', 6), ('Chunibala Devi', 6), ('Leonard Lucieer', 6)]
Kevin Bacon min:	 [('Kevin Bacon', 0), ('Walter Matthau', 1), ('Tony Romano', 1), ('Kimberly Scoll', 1), ('Michael deLeon', 1), ('Olympia Dukakis', 1), ('Tom Cruise', 1), ('Joshua Rodoy', 1), ('Tommy Lee Jones', 1), ('Teri Hatcher', 1)]
```

### Issues

#### Actor "s a"

In the provided csv file, there is an actor represented by string "s a". A long time I thought it was a single actor, whose name contained some characters that got systematically malformed while converting to csv from raw html, hence e.g. encoding issues.

Processing the graph further, he turned out to be easily the single most important actor. Comparing the cast list to IMDB list I found out its (probably) not a single actor, which was the reason why he was so important in the graph -- the error presumably got into more movies than the most successful actors in the set.

I ended up just disqualifying him from the data.

#### Messy plots

The plotting was (to me) surprisingly difficult to compute, so this was when I decided to trim the main cast graph for plotting purposes. I also faced performance issues with community detection.

It turned out that plotting is possible with rather a large graph (hundreds of distinct edges), but it serves no purpose. Plots of graphs with as few as 60+ distinct edges are so illegible that they are just useless. I ended up using 50 edges for the result plots.

### Comments on results

Looking at the data set, I was knew I actually know very few actors. However I expected to see some famous names coming from the centrality key players. Most of the actors are probably from the older movies.

The whole centrality topic still seems too abstract and hard to grasp. Comparing to the communities, where the plot literally speaks for itself, I find the centrality and its plots still very hard to interpret.